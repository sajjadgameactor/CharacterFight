Characters Fight is a card game made with unity engine for academic usage. Creating a balance between cards is the main application for which it is intended.
this is the main version which contain a normal GUI but another version of the project without GUI is available from below url:
* https://gitlab.com/sajjadgameactor/CharactersFightMassiveSimulator


In that version by removing GUI the game can be simulated Several thousand times per minute.
we use this simulation result to Check out the balance of the game by calculating win rate of each card. but you can test your own balancing algorithm with this game.

the project contain a file named "Cardinfo", all cards attribute is defined in this file and you can change that value of each attribute in that file 
current version of game contain of 50 cards based on famous characters but we are currently working on a new version of the game with more than hundred card Along with more sophisticated mechanics like Hearthstone so the balancing result will be more reliable.

The project contain 2 scene

Menu scene: you can customize your deck in this scene


![Menu scene](ReadmeFiles/Menu.JPG)

Game Scene: The game process runs on this scene. if you don't choose any deck. the game will initialize by random deck in this scene.
In the "GameLogicScript" file in the start function you can choose each player type (manual or AI) by passing PlayerType argument.

```c#
 player1 = new PlayerLogicScript(Turn.Player1, 0, CardsName.LeaderBatman, PlayerType.Manual);
 player2 = new PlayerLogicScript(Turn.Player2, 1000, CardsName.LeaderElsa, PlayerType.LocalAI);
```

![Game scene](ReadmeFiles/gameGUI.JPG)