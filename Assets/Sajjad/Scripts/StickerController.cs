﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;

public class StickerController : MonoBehaviour {

    [SerializeField]
    GameObject panelSticker;

    [SerializeField]
    Image stickerImage;

    [SerializeField]
    List<Sprite> stickersList;

    [SerializeField]
    List<AudioClip> stickerSound;

    AudioSource stickerAudioPlayer;

    bool isStickerPanelShow = false;

    void Awake()
    {
        stickerAudioPlayer = GetComponent<AudioSource>();
    }


    public void SendSticker(int stickerIndex)
    {

        stickerImage.sprite = stickersList[stickerIndex];
        ShowStickerPanel();
        
        stickerImage.gameObject.SetActive(true);
        stickerImage.transform.localScale = Vector3.zero;
        stickerImage.transform.DOScale(1, 0.5f).OnComplete(() => {
            stickerImage.transform.DOShakeRotation(1, 5).OnComplete(() =>
            {
                stickerImage.gameObject.SetActive(false);

            });

        });

        //try
        {
            if (stickerSound[stickerIndex] != null)
                stickerAudioPlayer.PlayOneShot(stickerSound[stickerIndex]);
            else
                Debug.LogWarning("No Audio for sticker " + stickerIndex.ToString());

        }
       
    }


    public void ShowStickerPanel()
    {
        isStickerPanelShow = !isStickerPanelShow;

        panelSticker.gameObject.SetActive(isStickerPanelShow);
    }

}
