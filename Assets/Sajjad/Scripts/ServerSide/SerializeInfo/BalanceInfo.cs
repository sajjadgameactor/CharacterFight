﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine.Serialization;
using System;
using LitJson;

[Serializable]
public class BalanceInfo
{

    public int win = 0;
    public int lose = 0;
  
}
