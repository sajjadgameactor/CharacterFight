﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCardScript : MonoBehaviour {

    [SerializeField]
    public Text manaText, powerText, HealthText,nameText;
    [SerializeField]
    Image cardImage;

    [HideInInspector]
    public CardInfo myInfo { get; private set; }

    // Use this for initialization
    void Start () {
		
	}

    public void SetCard(CardInfo info)
    {
        myInfo = info;
        SetImage(info.name);
        manaText.text = info.mana.ToString();
        powerText.text = info.power.ToString();
        HealthText.text = info.maxHealth.ToString();
        nameText.text = CardInfo.GetPersianName(info.name).ToString();
    }

    public void SetImage(CardsName name)
    {
        Sprite temp = Resources.Load<Sprite>(@"CardSprite\" + myInfo.name.ToString());
        if (temp != null)
        {
            cardImage.sprite = temp;
        }
        else
        {
            Debug.LogWarning("Card Image not found: " + myInfo.name);
        }
    }

   
}
