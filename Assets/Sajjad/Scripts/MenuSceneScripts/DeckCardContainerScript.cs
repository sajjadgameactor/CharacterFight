﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckCardContainerScript : MonoBehaviour {

    [SerializeField]
    Transform cardParent;

    [HideInInspector]
    public MenuCardScript myCard;

    [SerializeField]
    public Button button;

	public void InitPanel(CardsName name)
    {
        myCard = Instantiate(Resources.Load<MenuCardScript>("MenuCard")).GetComponent<MenuCardScript>();
        myCard.SetCard(new CardInfo(name, 0,Turn.Player1));
        myCard.transform.SetParent(cardParent);
    }

}
