﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;

[RequireComponent((typeof(Animator)))]
public class PanelCardManager : MonoBehaviour
{

    [SerializeField]
    Transform cardPanelContent, deckPanelContent;

    [SerializeField]
    MessegeBoxScript messegeBox;

    Dictionary<CardsName, CardContainerScript> cardContainersDic = new Dictionary<CardsName, CardContainerScript>(64);
    List<DeckCardContainerScript> deckCardContainerList = new List<DeckCardContainerScript>(30);

    Animator animator;

    Turn _selectedPlayer = Turn.Player1;

    Turn selectedPlayer
    {
        get { return _selectedPlayer; }
        set
        {
            _selectedPlayer = value;
            ResetDeck();
        }
    }


    [SerializeField]
    Text DeckCardNumberText,discriptionTitle,discriptionContent,playerText;

    void Awake()
    {
        animator = GetComponent<Animator>();

        //Vector2 mainWindow = GetMainGameViewSize();
        
        //GridLayoutGroup gridLayout = cardPanelContent.GetComponent<GridLayoutGroup>();
        //gridLayout.cellSize = new Vector2(Camera.main.pixelWidth / 7, Camera.main.pixelHeight / 3);
        //gridLayout.spacing = new Vector2(Camera.main.pixelWidth / 12, Camera.main.pixelHeight / 5);

        //GridLayoutGroup gridLayoutSelected = deckPanelContent.GetComponent<GridLayoutGroup>();
        //gridLayoutSelected.cellSize = new Vector2(Screen.width / 10, Screen.height / 4);
        //gridLayoutSelected.spacing = new Vector2(Screen.width / 9, Screen.height / 4);
        // PlayerPrefs.GetString()
    }

    public static Vector2 GetMainGameViewSize()
    {
        System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
        System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
        return (Vector2)Res;
    }


    // Use this for initialization
    void Start()
    {
        LoadCards();
        LoadDeck();
    }

    public void SaveDeck()
    {
        {
            string listString = "";
            foreach (var item in deckCardContainerList)
            {
                listString += ((int)item.myCard.myInfo.name).ToString() + " ";
            }

            switch (_selectedPlayer)
            {
                case Turn.Player1:
                    PlayerPrefs.SetString(Constant.player1Key + Constant.deckKey, listString);
                    break;
                case Turn.Player2:
                    PlayerPrefs.SetString(Constant.player2Key + Constant.deckKey, listString);
                    break;
                default:
                    break;
            }
        }

    }

    void LoadCards()
    {
        foreach (CardsName item in Enum.GetValues(typeof(CardsName)))
        {
            if ((int)item > 0 && (int)item < 1000)
            {
                CardContainerScript cardContainer = Instantiate(Resources.Load<GameObject>("CardContainer")).GetComponent<CardContainerScript>();
                CardInfo temp = new CardInfo(item, 0,Turn.Player1);
                cardContainer.InitPanel(temp);
                cardContainer.transform.SetParent(cardPanelContent);
                cardContainersDic.Add(item, cardContainer);
                cardContainer.button.onClick.AddListener(() => OnAddCardRequest(cardContainer));
                cardContainer.questionButton.onClick.AddListener(() => ShowDescription(cardContainer.myCard.myInfo));
            }
        }
    }

    void ResetDeck()
    {
        while (deckCardContainerList.Count != 0)
        {
            OnRemoveDeckCardRequest(deckCardContainerList[0]);
        }

        LoadDeck();
    }


    void LoadDeck()
    {
        string deckString = "";

        switch (_selectedPlayer)
        {
            case Turn.Player1:
                deckString = PlayerPrefs.GetString(Constant.player1Key + Constant.deckKey);
                break;
            case Turn.Player2:
                deckString = PlayerPrefs.GetString(Constant.player2Key + Constant.deckKey);
                break;
            default:
                break;
        }

        List<CardsName> deckCardsName = new List<CardsName>(); ;

        if (deckString != string.Empty)
        {
            char[] arrOperators = { ' ' };
            string[] selectedCardNameInt = deckString.Split(arrOperators, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                if (selectedCardNameInt.Length != 0)
                {
                    foreach (var item in selectedCardNameInt)
                    {
                        int temp = int.Parse(item);

                        deckCardsName.Add((CardsName)temp);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        else
        {
            //deckCardsName = LoadRandomDeck();
        }

        FillDeckPanel(deckCardsName);

    }

    List<CardsName> LoadRandomDeck()
    {
        List<CardsName> randomDeck = new List<CardsName>(30);

        for (int i = 0; i < 30; i++)
        {
            randomDeck.Add((CardsName)Random.Range(1, 39));
        }

        return randomDeck;

    }


    void FillDeckPanel(List<CardsName> deckCardsName)
    {
        foreach (var item in deckCardsName)
        {
            AddCard2DeckList(item);
        }
    }

    void AddCard2DeckList(CardsName name)
    {
        DeckCardContainerScript deckCardContainer = Instantiate(Resources.Load<GameObject>("DeckCardContainer")).GetComponent<DeckCardContainerScript>();
        deckCardContainer.InitPanel(name);
        deckCardContainer.transform.SetParent(deckPanelContent);
        deckCardContainer.button.onClick.AddListener(() => OnDeckCardClick(deckCardContainer)); // remove button
        cardContainersDic[name].Set1LightOn();
        deckCardContainerList.Add(deckCardContainer);
        DeckCardNumberText.text = deckCardContainerList.Count.ToString() + " /30";

    }

    public void OnDeckCardClick(DeckCardContainerScript deckCardContainer)
    {
        OnRemoveDeckCardRequest(deckCardContainer);

    }

    public void ShowSelectedCardPanel()
    {
        animator.SetBool("ShowSelectedPanel", !animator.GetBool("ShowSelectedPanel"));
    }

    public void OnAddCardRequest(CardContainerScript cardContainer)
    {
        ShowDescription(cardContainer.myCard.myInfo);

        if (deckCardContainerList.Count < 30)
        {
            if (cardContainer.HasCapacity())
            {
                AddCard2DeckList(cardContainer.myCard.myInfo.name);

            }
            else
            {
                messegeBox.ShowMessegeBox("حداکثر تعداد ممکن از این کارت را برداشته اید.".faConvert());

            }
        }
        else
        {
            messegeBox.ShowMessegeBox("دسته کارت کامل است".faConvert());
        }


    }

    public void OnDescriptionRequest(CardContainerScript cardContainer)
    {
        ShowDescription(cardContainer.myCard.myInfo);
    }

    public void OnRemoveDeckCardRequest(DeckCardContainerScript deckCardContainer)
    {
        deckCardContainerList.Remove(deckCardContainer);
        cardContainersDic[deckCardContainer.myCard.myInfo.name].Set1LightOff();
        DeckCardNumberText.text = deckCardContainerList.Count.ToString() + " /30";
        Destroy(deckCardContainer.gameObject);
    }

    public void OnChangePlayer(int player)
    {
        SaveDeck();

        switch (player)
        {
            case 1:
                selectedPlayer = Turn.Player1;
                playerText.text = "بازیکن 1".faConvert();
                break;
            case 2:
                selectedPlayer = Turn.Player2;
                playerText.text = "بازیکن 2".faConvert();
                break;
            default:
                selectedPlayer = Turn.Player1;
                Debug.LogError("What!!!");
                break;
        }
    }

   void ShowDescription(CardInfo card)
    {
        discriptionTitle.text =CardInfo.GetPersianName(card.name).ToString() + ("کارت").faConvert();
        discriptionContent.text = card.GetDiscription();
    }
}
