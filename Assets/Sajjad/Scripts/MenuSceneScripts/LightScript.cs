﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightScript : MonoBehaviour {

    [SerializeField]
    Image myImage;
  
    void Awake()
    {
        myImage.color = Color.black;
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetOn()
    {
        myImage.color = Color.white;
    }

    public void SetOff()
    {
        myImage.color = Color.black;
    }
}
