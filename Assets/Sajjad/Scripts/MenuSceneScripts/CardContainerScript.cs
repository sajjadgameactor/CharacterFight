﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardContainerScript : MonoBehaviour {

    
    [SerializeField]
    Transform cardPanel,lightPanel;

    List<LightScript> lights = new List<LightScript>();

    int onLightsNumber = 0;

    [SerializeField]
    public Button button, questionButton;

    [HideInInspector]
    public MenuCardScript myCard { get; private set; }

    // Use this for initialization
    void Start () {
		
	}

    public void InitPanel(CardInfo info)
    {
        myCard = Instantiate(Resources.Load<MenuCardScript>("MenuCard")).GetComponent<MenuCardScript>();
        myCard.SetCard(info);
        myCard.transform.SetParent(cardPanel);

        for (int i = 0; i < info.maxSelectable; i++)
        {
            GameObject light = Instantiate(Resources.Load<GameObject>("Light"));
            light.transform.SetParent(lightPanel);
            lights.Add(light.GetComponent<LightScript>());
        }
    }

    public bool HasCapacity()
    {
        if(onLightsNumber < myCard.myInfo.maxSelectable)
        {
            return true;
        }

        return false;
    }

    public void Set1LightOn()
    {
        if(onLightsNumber < lights.Count)
        {
            lights[onLightsNumber++].SetOn();
        }
        else
        {
            Debug.LogWarning("too much light!!!");
        }
    }


    public void Set1LightOff()
    {
        if(onLightsNumber > 0)
        {
            lights[--onLightsNumber].SetOff();
        }
        else
        {
            Debug.LogWarning("No light is On!!!");
        }
    }
}
