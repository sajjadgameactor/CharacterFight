﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{

    [SerializeField]
    GameObject battlePanel, CardPanel, marketPanel;

    [SerializeField]
    Button battleTabButtan, CardTabButton, MarketTabButton;

    Button selectedTabButton;

    [SerializeField]
    PanelCardManager panelCardManager;

    [SerializeField]
    MessegeBoxScript messegeBox;

    void Start()
    {
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 1;

        OnChangeTab(1);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="tab"></param>
	public void OnChangeTab(int tab)
    {
        if (selectedTabButton != null)
        {
            LayoutElement oldSelectedlayoutElement = selectedTabButton.GetComponent<LayoutElement>();

            DOTween.To(() => oldSelectedlayoutElement.flexibleWidth, x => oldSelectedlayoutElement.flexibleWidth = x, 1, 0.5f);
        }

        if (selectedTabButton != null && selectedTabButton == CardTabButton)
            panelCardManager.SaveDeck();

        switch (tab)
        {
            case 1: // battle
                selectedTabButton = battleTabButtan;
                battlePanel.SetActive(true);
                CardPanel.SetActive(false);
                marketPanel.SetActive(false);
                break;
            case 2: // card
                selectedTabButton = CardTabButton;
                battlePanel.SetActive(false);
                CardPanel.SetActive(true);
                marketPanel.SetActive(false);
                break;
            case 3: // market
                selectedTabButton = MarketTabButton;
                battlePanel.SetActive(false);
                CardPanel.SetActive(false);
                marketPanel.SetActive(true);
                break;
            default:
                throw new System.Exception("Tab not defined yet");
        }

        LayoutElement layoutElement = selectedTabButton.GetComponent<LayoutElement>();
        DOTween.To(() => layoutElement.flexibleWidth, x => layoutElement.flexibleWidth = x, 2, 0.5f);
    }

    public void OnBattleRequest()
    {
        SceneManager.LoadScene("gameScene");

        //if (HasComplateDeck(Turn.Player1))
        //{
        //    if(HasComplateDeck(Turn.Player2))
        //    {
        //        SceneManager.LoadScene("gameScene");
        //    }
        //    else
        //    {
        //        messegeBox.ShowMessegeBox("دسته کارت بازیکن دوم کامل نیست".faConvert());

        //    }
        //}
        //else
        //{
        //    messegeBox.ShowMessegeBox("دسته کارت بازیکن اول کامل نیست".faConvert());
        //}

    }


    public bool HasComplateDeck(Turn player)
    {
        string deckString = "";

        switch (player)
        {
            case Turn.Player1:
                deckString = PlayerPrefs.GetString(Constant.player1Key + Constant.deckKey);
                break;
            case Turn.Player2:
                deckString = PlayerPrefs.GetString(Constant.player2Key + Constant.deckKey);
                break;
            default:
                break;
        }

        if (deckString != string.Empty)
        {
            char[] arrOperators = { ' ' };
            string[] selectedCardNameInt = deckString.Split(arrOperators, StringSplitOptions.RemoveEmptyEntries);

            if (selectedCardNameInt.Length == 30)
                return true;
        }

        return false;
    }


}
