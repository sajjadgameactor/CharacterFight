﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ScoreNames
{
    HeadMiniGameScore,ControlBallScore
}

public class ScoreHandlerP2D:MonoBehaviour
{
    private const string secretKey = "I[_xS03(iS2gjf-32P1.}aho};Mug8c"; // Edit this value and make sure it's the same as the one stored on the server
    const string scoreKey = "helpInit";

    //public static string addScoreURL = "http://toopandwall.xzn.ir/addscore.php?"; //be sure to add a ? to your url
    public static string addScoreURL = "http://www.phoenix2d.ir/sixtygame/addscore.php?"; //be sure to add a ? to your url
    public static string rewardScoreURL = "http://www.phoenix2d.ir/sixtygame/getReward.php?"; //be sure to add a ? to your url

    [SerializeField]
    Text ScoreText;

    [SerializeField]
    ScoreNames scoreName;

    public delegate void ScoreChangeEventHandler(int point);

    public event ScoreChangeEventHandler OnScoreChange;


    private void ScoreChangeEvent()
    {
        //OnPushPoint?.Invoke(CurrentScore);
        if (OnScoreChange != null)
        {
            OnScoreChange(CurrentScore);
        }
    }

    private int showScore = 0;
    private int __CurrentScore = 0;
    int CurrentScore
    {
        get { return __CurrentScore; }
        set
        {
            __CurrentScore = value;
            ScoreChangeEvent();
            if (ScoreText != null)
            {
                ScoreText.text = value.ToString();

            }
        }
    }

    private int highScore=0;
    public int HighScore
    {
        get { return highScore; }
    }


    public int TotalScore
    {
        get { return CurrentScore; }
    }

    public int ShowScore
    {
        get { return showScore; }
    }

    void Start()
    {
        //if (SettingP2D.isHighScoreOnServer == 0)
        //{
        //    PostScore();
        //}

    }


    public void Reset(int chapter,int level)
    {
        SetHighScoreFromDb();
        CurrentScore = showScore = 0;
    }

    public void QuickScoreCalc()
    {
        showScore = TotalScore;
    }


    public bool pushPoint(int amount)
    {
        CurrentScore += amount;
        return true;
    }

    /// <summary>
    /// if we have enough Point Reduces the amount of required and return True else return false
    /// </summary>
    /// <param name="amount">required amount</param>
    /// <returns></returns>
    public bool popPoint(int amount)
    {
        if (CurrentScore < amount)
        {
            return false; // we have not enough Score
        }

        CurrentScore -= amount;

        return true;
    }

    void Update()
    {
        if (showScore == CurrentScore)
            return;
        else if (CurrentScore > showScore)
        {
            if (CurrentScore - showScore > 1000)
                showScore += 50;
            else if (CurrentScore - showScore > 100)
                showScore += 10;
            else
                showScore++;
        }
        else
            showScore--;
    }

    public void SetHighScoreFromDb()
    {
        //highScore = SettingP2D.GetCurrentLevelHighScore(LevelScript.currentChapter, LevelScript.currentLevel);

    }

    public void sendToHighScore(int medalNumbers)
    {
        highScore = CurrentScore;

        //SettingP2D.setCurrentLevelHighScore(LevelScript.currentChapter, LevelScript.currentLevel,highScore);

        //if(LevelScript.currentLevel == 0)
        //{
        //    SettingP2D.isHighScoreOnServer = 0;
        //    PostScore();
        //}
       
    }

    //public void saveVal(int val)
    //{
    //    string tmpV = P2DSecurety.Md5Sum(val.ToString());
    //    PlayerPrefs.SetString(scoreHashString, tmpV);
    //    PlayerPrefs.SetInt(scoreString, val);
    //}

    public void PostScore()
    {
        Debug.Log("post Score");
        string name;
        if (!PlayerPrefs.HasKey("Name"))
        {
            PlayerPrefs.SetString("Name", SystemInfo.deviceName);
        }
        name = PlayerPrefs.GetString("Name");
        int highScoreToSend =0;
        //P2DSecurety.SecureLocalLoad(currentLevelScoreKey, out highScoreToSend);
        string scoreHash = P2DSecurety.Md5Sum(highScoreToSend.ToString());
        string sholudHash = SystemInfo.deviceUniqueIdentifier + name + highScoreToSend.ToString() + scoreHash  + secretKey;
        string hash = P2DSecurety.Md5SumPHP(sholudHash);

        string post_url = addScoreURL + "device_id=" + SystemInfo.deviceUniqueIdentifier + "&name=" + WWW.EscapeURL(name) + "&score=" + highScoreToSend.ToString() + "&score_hash=" + scoreHash + "&hash=" + hash;

        StartCoroutine(__PostMessege(post_url));
    }

    // remember to use StartCoroutine when calling this function!
    private static IEnumerator __PostMessege(string messege)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.


        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(messege);
        yield return hs_post; // Wait until the download is done

        if (hs_post.error != null)
        {
            print("There was an error posting the high score: " + hs_post.error);
            SettingP2D.isHighScoreOnServer = 0;
        }
        else
        {
            if (hs_post.text == "ok")
            {
                SettingP2D.isHighScoreOnServer = 1;
                Debug.Log("OK");
            }
            else
            {
                Debug.Log(hs_post.text);
            }
        }
    }

    private IEnumerator _GrabReward()
    {

        WWW RankRewardAttempt = new WWW(rewardScoreURL + "device_id=" + SystemInfo.deviceUniqueIdentifier);

        yield return RankRewardAttempt;

        if (RankRewardAttempt.error == null)
        {

            int playerReward = 0;
            int.TryParse(RankRewardAttempt.text,out playerReward);
            if (playerReward != 0)
            {
                Debug.Log("Reward: " + playerReward);
                //StoreInventory.GiveItem(SixtyAssets.Coin_CURRENCY_ITEM_ID, playerReward);
            }
            else
            {
                Debug.Log("No Reward");

            }

        }
        else
        {
            print("There was an error grab reward: " + RankRewardAttempt.error);
        }
    }

}
