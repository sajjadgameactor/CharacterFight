﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public delegate void SavePlayerDelegate();

public class SavePlayerPanelScript : MonoBehaviour {

    [SerializeField]
    Image buttonImage;
    [SerializeField]
    Text messegeText;

    [SerializeField]
    GameObject panelInAppPaurches;

    Text buttonText;

    public event SavePlayerDelegate OnSavePlayerEvent;

    const int firstSaveBuyCoinAmount = 125;
    int currentBuyCoinAmount = 125;

    float buttonSavePlayerSecound = 4;
    float elepsedTimeButtonTime = 0;

    public bool Started = false;

	// Use this for initialization
	void Start () 
    {
        buttonText = buttonImage.transform.Find("Text").GetComponent<Text>();
        
        //inAppPurchesPanelP2D.OnPanelEndEvent += inAppPurchesPanel_OnPanelEndEvent;
        messegeText.text = ("پرتقال اضافی".faConvert());

        StartInit();
	}

    //void inAppPurchesPanel_OnPanelEndEvent()
    //{
    //    LevelScript.gameState = LevelScript.GameState.timeOver;
    //}

    public void StartInit()
    {
        currentBuyCoinAmount = firstSaveBuyCoinAmount;
        SetButtonText();
    }

    private void doubleCoinAmount()
    {
        currentBuyCoinAmount = currentBuyCoinAmount * 2;
        SetButtonText();
    }

    private void SetButtonText()
    {
        //buttonText.text = (currentBuyCoinAmount) + " "  + GeleAssets.currencyName + " ";
    }
	
	// Update is called once per frame
    void FixedUpdate()
    {
        //if (Started && LevelScript.gameState == LevelScript.GameState.SavePanel)
        //{
        //    elepsedTimeButtonTime += Time.fixedDeltaTime;

        //    buttonImage.fillAmount = 1 - (elepsedTimeButtonTime / buttonSavePlayerSecound);

        //    if (elepsedTimeButtonTime > buttonSavePlayerSecound)
        //    {
        //        ClosePanel();
        //    }
        //}
    }

    private void ClosePanel()
    {
        elepsedTimeButtonTime = 0;
        Started = false;
        GetComponent<P2DPanel>().Hide();
    }

    public void BuyTimeButtonClick()
    {
        //if (StoreInventory.GetItemBalance(GeleAssets.Gele_CURRENCY_ITEM_ID) > currentBuyCoinAmount)
        //{

        //    SavePlayer();

        //    ClosePanel();

        //    doubleCoinAmount();
        //}
        //else
        //{
        //    LevelScript.gameState = LevelScript.GameState.inAppPaurches;
        //    elepsedTimeButtonTime = 0;
        //   SettingP2D.inAppPurches.Show();
        //}
       
    }

    private void SavePlayer()
    {
        if (OnSavePlayerEvent != null)
        {
            OnSavePlayerEvent();
        }
    }
}

