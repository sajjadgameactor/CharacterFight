﻿using UnityEngine;

static public class MethodExtensionForMonoBehaviourTransform
{
    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
    /// </summary>
    static public T GetOrAddComponent<T>(this Component child) where T : Component
    {
        T result = child.GetComponent<T>();
        if (result == null)
        {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }

    public static void SetParentAndReset(this RectTransform rect, Transform parent)
    {
        rect.SetParent(parent);
        rect.localScale = Vector3.one;
        rect.localPosition = Vector3.zero;
    }
}