﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LeaderScript : AttakableObjectScript {

    SpriteRenderer mySprite;

    [SerializeField]
    public Text  HealthText,bloodText;

    [SerializeField]
    CanvasGroup bloodCanvas;

    [SerializeField]
    Transform attackPos;

    [SerializeField]
    SpriteRenderer outlineRenderer;

    void Awake()
    {
        mySprite = GetComponent<SpriteRenderer>();
    }

    public void SetLeader(CardInfo info,PlayerScript player)
    {
        base.SetObject(info, player);

    }

    public override void SetHealth(int value)
    {
        base.SetHealth(value);
        UpdateHealthText();

    }

    public override void Heal(int value)
    {
        base.Heal(value);
        UpdateHealthText();

    }


    private void UpdateHealthText()
    {
        HealthText.text = myInfo.currentHealth.ToString("f0");

    }

    public override void OnAttackThreat()
    {
        mySprite.color = onAttackColor;

    }

    public override void OnAttackElimination()
    {
        mySprite.color = Color.white;
    }

    Tweener attackTween;
    public override bool Impact(int attackPower)
    {
        bloodText.text = "-" + attackPower.ToString();
        if(attackTween != null && !attackTween.IsComplete())
        {
            Debug.LogWarning("kill tween");
            attackTween.Complete();
        }

        attackTween =  DOTween.To(() => bloodCanvas.alpha, x => bloodCanvas.alpha = x, 1, 0.2f).OnComplete(() =>
          {
              attackTween = DOTween.To(() => bloodCanvas.alpha, x => bloodCanvas.alpha = x, 0, 1).SetDelay(2);
          });

        bool result = base.Impact(attackPower);
        UpdateHealthText();

        return result;
    }

    public override void ShowOutline()
    {
        outlineRenderer.enabled = true;
    }

    public override void HideOutLine()
    {
        outlineRenderer.enabled = false;
    }

    public override Vector3 GetAttackPosition()
    {
        return attackPos.position;
    }
}
