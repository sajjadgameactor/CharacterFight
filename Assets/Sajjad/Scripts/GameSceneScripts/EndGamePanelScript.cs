﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameResult
{
    victory,Fail
}

public class EndGamePanelScript : MonoBehaviour {

    [SerializeField]
    Text resultText;

    public void ShowPanel(GameResult result)
    {
        gameObject.SetActive(true);

        switch (result)
        {
            case GameResult.victory:
                resultText.text = "پیروزی".faConvert();
                break;
            case GameResult.Fail:
                resultText.text = "شکست".faConvert();
                break;
            default:
                break;
        }
    }

    public void OnEndGameClick()
    {
        SceneManager.LoadScene(Constant.SceneMenu);
    }


}
