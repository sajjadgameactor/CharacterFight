﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class OnGroundCardManagerScript : MonoBehaviour {

    public List<CardScript> groundCard = new List<CardScript>(6);

    [SerializeField]
    Vector3 centerHand;

    float cardGap = 1.75f;

 
    [SerializeField]
    Vector3 cardRotation = new Vector3(0, 0, 0);
    public void AddCard2ground(CardScript card)
    {
        SetOutline(card);

        groundCard.Add(card);
        card.transform.SetParent(this.transform);
        card.transform.DORotate(cardRotation, 0.5f);
        ReOrderGround();
    }

    private void SetOutline(CardScript card)
    {
        if (card.myInfo.CanAttack())
            card.ShowOutline();
    }

    public void HideAllOutline()
    {
        foreach (var item in groundCard)
        {
            item.HideOutLine();
        }
    }

    public void ReOrderGround(int shadowPosition = -1) //TODO: shadowPosition not used yet
    {
        float startX = centerHand.x - (((float)(groundCard.Count - 1) / 2) * cardGap);

        for (int i = 0; i < groundCard.Count; i++)
        {
            //groundCard[i].transform.DOKill();
            groundCard[i].onGroundPosition = new Vector3(startX + (i * cardGap), centerHand.y, centerHand.z);
            groundCard[i].Go2GroundPosition();
        }
    }

    public CardScript FetchCard(int cardID)
    {
        CardScript temp = FindCard(cardID);
        groundCard.Remove(temp);
        return temp;
    }

    public CardScript FindCard(int cardID)
    {
       return groundCard.Find(x => x.myInfo.cardID == cardID);
    }
    public void SetCardsDragable(bool value)
    {
        foreach (var item in groundCard)
        {
            item.SetDragable(value);
        }
    }

    public void RemoveCard(CardScript card)
    {
        groundCard.Remove(card);
        Destroy(card.gameObject);
    }

    public bool HasCapacity()
    {
        if (groundCard.Count < 6)
            return true;

        return false;
    }

    public void Go2NextTurn()
    {
        foreach (var item in groundCard)
        {
            item.myInfo.Go2NextTurn();

            SetOutline(item);
        }
    }

}
