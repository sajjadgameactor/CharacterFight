﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class HandDeckControllerScript : MonoBehaviour {

    List<CardScript> handCard = new List<CardScript>();

    [SerializeField]
    Vector3 StartHandPosition;

    const float maxGap = 1.7f;

    public bool Gap2Right = false;

    [SerializeField]
    Vector3 cardRotation ;


    public void AddCard2Hand(CardScript card)
    {
        handCard.Add(card);
        card.transform.SetParent(this.transform);
        ReOrderHand();
    }
    
    public void ReOrderHand()
    {
        float cardGap = maxGap;
        Vector3 currentRotate = new Vector3(0,0, cardRotation.z);
        if (handCard.Count > 5)
        {
            cardGap = Mathf.Max(0.5f, maxGap - ((float)(handCard.Count - 2) / 8));
            currentRotate = cardRotation;
        }

        if (Gap2Right)
            cardGap = -cardGap;


        for (int i = 0; i < handCard.Count; i++)
        {
            int ki = handCard[i].transform.DOKill();
            handCard[i].transform.DOMove(new Vector3(StartHandPosition.x - (i * cardGap),StartHandPosition.y,StartHandPosition.z),5).SetSpeedBased(true);
            handCard[i].transform.DORotate(currentRotate, 0.5f);
        }
    }

    public CardScript FetchCard(int  cardID)
    {
        CardScript temp = handCard.Find(x => x.myInfo.cardID == cardID);
        if (temp != null)
        {
            handCard.Remove(temp);
            ReOrderHand();
        }
        return temp;
    }
}
