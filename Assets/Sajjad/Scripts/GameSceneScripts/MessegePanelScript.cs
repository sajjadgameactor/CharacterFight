﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(CanvasGroup))]
public class MessegePanelScript : MonoBehaviour {

    [SerializeField]
    Text myText;

    CanvasGroup myCanvasGroup;

	// Use this for initialization
	void Start () {

        if (myText == null)
            Debug.LogError("Text not initilized");

        myCanvasGroup = GetComponent<CanvasGroup>();

        myCanvasGroup.alpha = 0;
	}
	

    public void ShowMessege(string messege,float duration)
    {
        myText.text = messege;

        DOTween.To(() => myCanvasGroup.alpha, x => myCanvasGroup.alpha = x, 1, 0.5f).OnComplete(() =>
        {
            DOTween.To(() => myCanvasGroup.alpha, x => myCanvasGroup.alpha = x, 0, 0.5f).SetDelay(duration);
        });

    }

   

}
