﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;

[RequireComponent(typeof(BoxCollider))]
public class CardScript : AttakableObjectScript {


    [SerializeField]
    public Text manaText, powerText, HealthText,descriptionText,bloodText,nameText;

    [SerializeField]
    Image cardImage,backImage,specialImage,EffectImage;

    [SerializeField]
    GraphicRaycaster graphicRaycaster;

    [SerializeField]
    public Button button;

    [SerializeField]
    CanvasGroup bloodCanvas;

    [SerializeField]
    Outline outline;

    [SerializeField]
    Transform attackPos;

    [HideInInspector]
    public bool isInAttack = false;

    [HideInInspector]
    public BoxCollider myColider;

    [HideInInspector]
    public Vector3 onGroundPosition;

    void Awake()
    {
        gameObject.SetActive(true);
        myColider = GetComponent<BoxCollider>();
        myColider.enabled = false;
    }

    public void SetCard(CardInfo info,PlayerScript Player,bool isUpdate = false)
    {
        base.SetObject(info, Player); 
        SetMana(info.mana);
        SetPower(info.power,info.extraPower);
        SetHealth(info.maxHealth);
        nameText.text = CardInfo.GetPersianName(info.name);

        if(!isUpdate)
            SetImage(info.name);

        if (myInfo.isHidden)
            specialImage.sprite = GameSceneScript.Instance.HiddenEffect;
        else if(myInfo.isDefender)
        {
            specialImage.sprite = GameSceneScript.Instance.defenderEffect;
            specialImage.color = new Color(0, 0, 0, 0.5f);
        }

    }

    public void SetImage(CardsName name)
    {
        Sprite temp = Resources.Load<Sprite>(@"CardSprite\" + myInfo.name.ToString());
        if (temp != null)
        {
            cardImage.sprite = temp;
        }
    }


    public void SetDragable(bool value)
    {
        switch (value)
        {
            case true:
                graphicRaycaster.enabled = true;
                break;
            case false:
                graphicRaycaster.enabled = false;
                break;
            default:
                break;
        }
    }

    public override void SetMana(int value)
    {
        base.SetMana(value);
        manaText.text = value.ToString("f0");
    }

    public override void SetPower(int power,int extra)
    {
        base.SetPower(power,extra);

        UpdatePowerText();
    }

    public override void AddExtra(int extra)
    {
        base.AddExtra(extra);

        UpdatePowerText();

    }

    private void UpdatePowerText()
    {
        powerText.text = (myInfo.power + myInfo.extraPower).ToString("f0");

    }

    public override void SetHealth(int value)
    {
        base.SetHealth(value);
        UpdateHealthText();
    }

    [HideInInspector]
    public AttakableObjectScript victamObject;
    void OnTriggerEnter(Collider coll)
    {
        if (isInAttack)
        {
            AttakableObjectScript newAttackable = coll.GetComponent<AttakableObjectScript>();
            if (newAttackable != null && !newAttackable.myInfo.isDead)
            {
                if (newAttackable.myPlayer != myPlayer)
                {
                    if (victamObject != null)
                        victamObject.OnAttackElimination();

                    victamObject = newAttackable;
                    victamObject.OnAttackThreat();
                }
            }
        }
       
    }

    void OnTriggerExit(Collider coll)
    {
        AttakableObjectScript collCard = coll.GetComponent<AttakableObjectScript>();
        if (collCard != null)
        {
            if(collCard.myPlayer != myPlayer)
            {
                if(victamObject == collCard)
                    victamObject = null;

                collCard.OnAttackElimination();
            }
        }
    }

    public void Go2GroundPosition()
    {
        transform.DOMove(onGroundPosition, 0.7f).SetEase(Ease.Linear);
    }

    private void ShowSpecialImage()
    {
        DOTween.To(() => specialImage.fillAmount, x => specialImage.fillAmount = x, 1, 1);
    }

    private void HideSpecialImage()
    {
        DOTween.To(() => specialImage.fillAmount, x => specialImage.fillAmount = x, 0, 1);
    }

    public override void OnAttackThreat()
    {
        backImage.color = onAttackColor;
    }

    public override void OnAttackElimination()
    {
        backImage.color = Color.white;

    }

    public override void Heal(int value)
    {
        base.Heal(value);
        UpdateHealthText();

    }

    private void UpdateHealthText()
    {
        HealthText.text = myInfo.currentHealth.ToString("f0");

    }

    Tweener attackTween;
    public override bool Impact(int attackPower)
    {
        bloodText.text = "-" + attackPower.ToString();

        if (attackTween != null && !attackTween.IsComplete())
        {
            Debug.LogWarning("kill tween");
            attackTween.Complete();
        }

        attackTween = DOTween.To(() => bloodCanvas.alpha, x => bloodCanvas.alpha = x, 1, 0.2f).OnComplete(() =>
        {
            attackTween = DOTween.To(() => bloodCanvas.alpha, x => bloodCanvas.alpha = x, 0, 1).SetDelay(2);
        });

        bool result = base.Impact(attackPower);
        UpdateHealthText();

        return result;
    }

    /// <summary>
    /// outline show this card has attack on this turn
    /// </summary>
    /// <param name="attackPower"></param>
    public override void ShowOutline()
    {
        outline.enabled = true;
    }

    public override void HideOutLine()
    {
        outline.enabled = false;

    }

    public override Vector3 GetAttackPosition()
    {
        return attackPos.position;
    }

    public void ShowFrozenEffect(int turn)
    {
        myInfo.Stun(turn);
        EffectImage.sprite = GameSceneScript.Instance.frozenEffect;
        DOTween.To(() => EffectImage.fillAmount, x => EffectImage.fillAmount = x, 1,1);
    }

    public void ShowSpiderWebEffect(int turn)
    {
        myInfo.Stun(turn);
        EffectImage.sprite = GameSceneScript.Instance.spiderWebEffect;
        DOTween.To(() => EffectImage.fillAmount, x => EffectImage.fillAmount = x, 1, 1);
    }

    public void HideEffect()
    {
        DOTween.To(() => EffectImage.fillAmount, x => EffectImage.fillAmount = x, 0, 1);

    }

    public void ShowHiddenSpecialAbility()
    {
        ShowSpecialImage();
    }

    public void ShowDefenderAbility()
    {
        ShowSpecialImage();
    }

    public void UnHideCard()
    {
        myInfo.isHidden = false;
        HideSpecialImage();
    }

}
