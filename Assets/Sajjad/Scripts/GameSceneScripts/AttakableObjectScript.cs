﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AttakableObjectScript:MonoBehaviour {

    protected static Color onAttackColor = new Color(0.85f, 0.55f, 0.55f);

    public CardInfo myInfo { get; private set; }
    public PlayerScript myPlayer { get; private set; }

    protected void SetObject(CardInfo info, PlayerScript Player)
    {
        myPlayer = Player;
        myInfo = info;
    }

    public abstract void OnAttackThreat();
    public abstract void OnAttackElimination();

    public abstract void ShowOutline(); // visual feedback which show this card can attack on current turn
    public abstract void HideOutLine(); // visual feedback which show this card can't attack any more current turn
    public abstract Vector3 GetAttackPosition(); 

    public virtual void SetMana(int newValue)
    {
        myInfo.mana = newValue;
    }

    public virtual void SetHealth(int newValue)
    {
        myInfo.SetHealth(newValue);
    }

    public virtual bool Impact(int power)
    {
        return myInfo.Impact(power);
    }
    public virtual void Heal(int power)
    {
         myInfo.Heal(power);
    }

    public virtual void SetPower(int newPower,int newExtra)
    {
        myInfo.power = newPower;
        myInfo.extraPower = newExtra;
    }

    public virtual void AddExtra(int extra)
    {
        myInfo.extraPower += extra;
    }
}
