﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using LitJson;
using System;
using BestHTTP;
using SimpleJSON;

public class GameSceneScript : Singleton<GameSceneScript>
{
    public static List<ItemMother> balancedItemList = new List<ItemMother>();

    public enum AspectRatio
    {
        Aspect_4_3,
        Aspect_16_10,
        Aspect_16_9,

    }

    public AspectRatio currentAspectRatio { get; private set; }

    public enum GameState
    {
        Connect, // stablish connection between players
        Start, // 3 secound for starting the game
        player1Turn, // Attacker have 10 secound for useing a card
        ChangeTurn,
        player2Turn,
        End
    }

    GameState _gameState;
    GameState gameState
    {
        get { return _gameState; }
        set
        {
            _gameState = value;
            switch (_gameState)
            {
                case GameState.Connect:
                    break;
                case GameState.Start:
                    break;
                case GameState.player1Turn:
                    break;
                case GameState.ChangeTurn:
                    break;
                case GameState.player2Turn:
                    break;
                case GameState.End:
                    break;
                default:
                    break;
            }
        }
    }

    [SerializeField]
    public MessegePanelScript messegePanel;
    [SerializeField]
    EndGamePanelScript endGamePanel;

    public GameLogicScript gameLogicScript;
    TurnManager localTurnManager;

    [SerializeField]
    public PlayerScript player1, player2;

    [SerializeField]
    Button turnButton;

    [HideInInspector]
    public PlayerScript currentAttacker, currentDefender, myPlayer;


    [Header("global sprite")]
    public Sprite frozenEffect;
    public Sprite HiddenEffect, defenderEffect, spiderWebEffect;

    void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        //gameLogicScript = new GameLogicScript(Turn.Player1);

        endGamePanel.gameObject.SetActive(false);
    }

    void FindAspectRatio()
    {
        float windowaspect = (float)Screen.width / (float)Screen.height;

        if (windowaspect < 1.4f)
        {
            currentAspectRatio = AspectRatio.Aspect_4_3;
            Camera.main.fieldOfView = 58;
        }
        else if (windowaspect < 1.7)
        {
            currentAspectRatio = AspectRatio.Aspect_16_10;
            Camera.main.fieldOfView = 50;

        }
        else
        {
            currentAspectRatio = AspectRatio.Aspect_16_10;
            Camera.main.fieldOfView = 50;
        }

    }

    // Use this for initialization
    void Start()
    {
        //StartCoroutine(GetGameData());
    }


    public void StartGame(string startInfoJson)
    {
        StartGameInfo startInfo = LitJson.JsonMapper.ToObject<StartGameInfo>(startInfoJson);

        player1.SetPlayer(Turn.Player1, startInfo.leader1, player2); //** should be before foreach loop because it set true turn for player
        player2.SetPlayer(Turn.Player2, startInfo.leader2, player1);

        myPlayer = player1;

        foreach (var item in startInfo.hand1)
        {
            player1.addCard2Hand(item);
        }

        foreach (var item in startInfo.hand2)
        {
            player2.addCard2Hand(item);
        }


        player1.mana = startInfo.mana1;
        player2.mana = startInfo.mana2;

        localTurnManager = new TurnManager();
        localTurnManager.OnTurnChange += LocalTurnManager_OnTurnChange;
        localTurnManager.SetTurn(startInfo.currentTurn);

    }

    private void LocalTurnManager_OnTurnChange(Turn currentTurn)
    {
        switch (currentTurn)
        {
            case Turn.Player1:
                currentAttacker = player1;
                currentDefender = player2;
                player1.ShowHand();
                player1.onGroundCardController.SetCardsDragable(true);
                player2.HideHand();
                player2.onGroundCardController.HideAllOutline();
                player2.onGroundCardController.SetCardsDragable(false);
                break;
            case Turn.Player2:
                currentAttacker = player2;
                currentDefender = player1;
                player1.HideHand();
                player1.onGroundCardController.HideAllOutline();
                player1.onGroundCardController.SetCardsDragable(false);
                player2.ShowHand();
                player2.onGroundCardController.SetCardsDragable(true);
                break;
            default:
                break;
        }

        currentAttacker.onGroundCardController.Go2NextTurn();
    }

    public void OnTurnChangeRequest()
    {
        switch (localTurnManager.currentTurn)
        {
            case Turn.Player1:
                turnButton.transform.DOMoveY(2.5f, 1);
                break;
            case Turn.Player2:
                turnButton.transform.DOMoveY(-2.4f, 1);
                break;
            default:
                break;
        }

        gameLogicScript.TurnDone(currentAttacker.myTurn);


    }
    public void AttackAcceptedByServer(string jsonInfos)
    {
        List<InteractionInfo> infos = JsonMapper.ToObject<List<InteractionInfo>>(jsonInfos);
        StartCoroutine(ShowInteractions(infos));
    }

    public void Hand2GroundAcceptedByServer(string jsonHand2GroundInfo)
    {
        Hand2GroundInfo hand2GroundInfo = JsonMapper.ToObject<Hand2GroundInfo>(jsonHand2GroundInfo);

        PlayerScript interactionAttacker;
        PlayerScript interactionDefender;

        switch (hand2GroundInfo.player)
        {
            case Turn.Player1:
                interactionAttacker = player1;
                interactionDefender = player2;
                break;
            case Turn.Player2:
                interactionAttacker = player2;
                interactionDefender = player1;
                break;
            default:
                throw new System.Exception("Undefined player!!!");
        }


        foreach (var item in hand2GroundInfo.added2HandCard)
        {
            interactionAttacker.addCard2Hand(item);
        }


        CardScript requestedCard = interactionAttacker.handController.FetchCard(hand2GroundInfo.cardID);
        if (requestedCard == null)
        {
            Debug.LogError("Card not found");
        }
        else
        {
            interactionAttacker.mana = hand2GroundInfo.newPlayerMana;
            requestedCard.myColider.enabled = true;
            requestedCard.button.onClick.RemoveAllListeners();

            EventTrigger trigger = requestedCard.button.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Drag;
            entry.callback.AddListener(delegate { MoveByMouse(requestedCard); });
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.BeginDrag;
            entry.callback.AddListener(delegate { BeginDragCard(requestedCard); });
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener(delegate { DropCard(requestedCard); });
            trigger.triggers.Add(entry);


            interactionAttacker.onGroundCardController.AddCard2ground(requestedCard);

            if (requestedCard.myInfo.isDefender)
            {
                requestedCard.ShowDefenderAbility();
            }
            if (requestedCard.myInfo.isHidden)
            {
                requestedCard.ShowHiddenSpecialAbility();
            }
        }

        StartCoroutine(ShowInteractions(hand2GroundInfo.interactionInfoOnEnterGround)); // should be after adding card on ground

    }

    private void BeginDragCard(CardScript card)
    {
        card.isInAttack = true;
        card.transform.DOMoveZ(1, 0.2f);
    }

    Vector3 mousePos = new Vector3();
    private void MoveByMouse(CardScript card)
    {
        mousePos.Set(Input.mousePosition.x, Input.mousePosition.y, card.transform.position.z - Camera.main.transform.position.z);
        Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(mousePos);
        card.transform.position = new Vector3(mouseWorld.x, mouseWorld.y, card.transform.position.z);
    }

    public Tweener tweenAttackerUp;
    private void DropCard(CardScript card)
    {
        card.isInAttack = false;

        if (card.victamObject != null)
        {
            if (card.myPlayer.CheckCanCardAttack(card))
            {
                ReadyCard4Attack(card);
                gameLogicScript.AttackCardRequest(card.myInfo.cardID, card.victamObject.myInfo.cardID, card.myPlayer.myTurn);
            }
            else
            {
                card.Go2GroundPosition();
            }

        }
        else
        {
            card.Go2GroundPosition();
        }
    }

    private void ReadyCard4Attack(CardScript card)
    {
        tweenAttackerUp = card.transform.DOMove(new Vector3(card.transform.position.x, card.transform.transform.position.y, 0), 0.5f);
    }

    private IEnumerator ShowInteractions(List<InteractionInfo> interactionInfos)
    {
        PlayerScript interactionAttacker;
        PlayerScript interactionDefender;

        foreach (var item in interactionInfos)
        {

            if (item.error == InteractionError.None)
            {
                switch (item.subjectPlayer)
                {
                    case Turn.Player1:
                        interactionAttacker = player1;
                        interactionDefender = player2;
                        break;
                    case Turn.Player2:
                        interactionAttacker = player2;
                        interactionDefender = player1;
                        break;
                    default:
                        throw new Exception("What!!!");
                }

                switch (item.interactionType)
                {
                    case InteractionType.Impact:
                        ShowImpact(interactionAttacker, interactionDefender, item);
                        yield return 0;
                        break;
                    case InteractionType.MagicalImpact:
                        MagicalImpact(interactionAttacker, interactionDefender, item);
                        yield return 0;
                        break;
                    case InteractionType.Frozen:
                        CardScript attacker = interactionAttacker.onGroundCardController.FindCard(item.subjectID);
                        foreach (var frozenItem in item.objectIDList)
                        {
                            CardScript victam = interactionDefender.onGroundCardController.FindCard(frozenItem);
                            victam.ShowFrozenEffect(item.Power1);
                        }

                        break;
                    case InteractionType.SpiderWeb:
                        foreach (var frozenItem in item.objectIDList)
                        {
                            CardScript victam = interactionDefender.onGroundCardController.FindCard(frozenItem);
                            victam.ShowSpiderWebEffect(item.Power1);
                        }
                        break;
                    case InteractionType.Heal:
                        foreach (var obj in item.objectIDList)
                        {
                            FindFromAllOnGround(obj).Heal(item.Power1);

                        }
                        break;
                    case InteractionType.RemoveStunEffect:
                        CardScript effectedCard = interactionAttacker.onGroundCardController.FindCard(item.objectIDList[0]);
                        effectedCard.myInfo.RemoveStunEffect();
                        effectedCard.HideEffect();
                        if (effectedCard.myInfo.CanAttack())
                            effectedCard.ShowOutline();
                        break;
                    case InteractionType.RemoveHiddenSpecialAbility:
                        foreach (var obj in item.objectIDList)
                        {
                            interactionDefender.onGroundCardController.FindCard(obj).UnHideCard();

                        }
                        break;
                    case InteractionType.ExtraPower:
                        foreach (var obj in item.objectIDList)
                        {
                            FindFromAllOnGround(obj).AddExtra(item.Power1);

                        }
                        break;
                    case InteractionType.Transform:
                        TransformCard(FindFromAllOnGround(item.objectIDList[0]), item.objectTransform);
                        //Debug.LogError("not implementerd");
                        break;
                    case InteractionType.NewCard:
                        AddInteractionNewCard2Hand(item);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (item.error)
                {
                    case InteractionError.None:
                        break;
                    case InteractionError.VictamIsDead:
                        messegePanel.ShowMessege("کارت قبلا نابود شده است".faConvert(), 2);
                        PlayerScript attacker = item.subjectPlayer == Turn.Player1 ? player1 : player2;
                        attacker.onGroundCardController.FindCard(item.subjectID).Go2GroundPosition();
                        break;
                    default:
                        break;
                }
            }

        }
    }

    AttakableObjectScript FindFromAllOnGround(int cardID)
    {
        AttakableObjectScript temp = player1.FindAttackableObject(cardID);
        if (temp == null)
            temp = player2.FindAttackableObject(cardID);

        return temp;
    }

    private void MagicalImpact(PlayerScript interactionAttacker, PlayerScript interactionDefender, InteractionInfo item)
    {
        CardScript attacker = interactionAttacker.onGroundCardController.FindCard(item.subjectID);
        AttakableObjectScript victam = interactionDefender.FindAttackableObject(item.objectIDList[0]);

        ImpactProcess(item, victam, attacker);
    }

    private void ShowImpact(PlayerScript interactionAttacker, PlayerScript interactionDefender, InteractionInfo item)
    {
        CardScript attacker = interactionAttacker.onGroundCardController.FindCard(item.subjectID);
        AttakableObjectScript victam = interactionDefender.FindAttackableObject(item.objectIDList[0]);

        attacker.myInfo.currentTurnAttackNumber--;
        if (attacker.myInfo.currentTurnAttackNumber == 0)
            attacker.HideOutLine();

        if (attacker.myInfo.isHidden && item.unHideSubject)
        {
            attacker.UnHideCard();
        }

        if (item.objectDead)
            victam.myInfo.isDead = true;

        if(tweenAttackerUp != null && tweenAttackerUp.IsActive())
        {
            tweenAttackerUp.OnComplete(() =>
            {
                AttackingAnimationDown(item, attacker, victam);

            });
        }
        else
        {
            AttackingAnimationDown(item, attacker, victam);

        }

    }

    private void AttackingAnimationDown(InteractionInfo item, CardScript attacker, AttakableObjectScript victam)
    {
        Vector3 attackPos = victam.GetAttackPosition();
        attacker.transform.DOMove(attackPos, 0.3f).SetEase(Ease.Linear).OnComplete(() =>
        {
            Camera.main.DOShakeRotation(0.3f, 1);
            ImpactProcess(item, victam, attacker);
            attacker.transform.DOMove(attacker.onGroundPosition, 0.7f).SetDelay(0.3f).OnComplete(() =>
            {
                if (item.subjectTransform != null)
                {
                    TransformCard(attacker, item.subjectTransform);
                }
                else
                {
                    if (item.subjectDead)
                    {
                        attacker.transform.DOScale(0.1f, 1).SetEase(Ease.InBack).OnComplete(() =>
                        {
                            attacker.myPlayer.onGroundCardController.RemoveCard(attacker);

                        });
                    }
                }
            });
        });
    }

    private void ImpactProcess(InteractionInfo item, AttakableObjectScript victam, AttakableObjectScript attacker)
    {
        victam.Impact(item.Power1);

        if (item.interactionType == InteractionType.Impact && !(victam is LeaderScript))
            attacker.Impact(item.Power2);

        AddInteractionNewCard2Hand(item);

        if (item.objectDead)
        {

            if (victam is LeaderScript)
            {
                EndGame(attacker.myPlayer.myTurn);
            }
            else
            {
                if (item.objectTransform != null)
                {
                    TransformCard(victam,item.objectTransform);
                }
                else
                {
                    victam.transform.DOScale(0.1f, 1).SetEase(Ease.InBack).OnComplete(() =>
                    {
                        victam.myPlayer.onGroundCardController.RemoveCard((CardScript)victam);

                    });
                }

            }
        }
    }

    private void AddInteractionNewCard2Hand(InteractionInfo item)
    {
        foreach (var card in item.newPlayersCard)
        {
            switch (card.myPlayer)
            {
                case Turn.Player1:
                    player1.addCard2Hand(card);
                    break;
                case Turn.Player2:
                    player2.addCard2Hand(card);
                    break;
                default:
                    break;
            }
        }

    }

    private static void TransformCard( AttakableObjectScript oldInfo, CardInfo newInfo)
    {
        oldInfo.transform.DOScale(0.1f, 1).SetEase(Ease.InBack).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
        {
            ((CardScript)oldInfo).SetCard(newInfo, oldInfo.myPlayer);

        });
    }

    public void InitNewTurn(string nextTurnJson)
    {
        NextTurnInfo nextTurnInfo = JsonMapper.ToObject<NextTurnInfo>(nextTurnJson);

        player1.mana = nextTurnInfo.mana1;
        player2.mana = nextTurnInfo.mana2;

        foreach (var item in nextTurnInfo.newPlayer1Card)
        {
            player1.addCard2Hand(item);
        }
        foreach (var item in nextTurnInfo.newPlayer2Card)
        {
            player2.addCard2Hand(item);
        }

        StartCoroutine(ShowInteractions(nextTurnInfo.interactionInfoAttacker));

        localTurnManager.SetTurn(nextTurnInfo.currentTurn, nextTurnInfo.currentTurnNumber);

      
    }

  
    public void EndGame(Turn winnerPlayer)
    {
        GameResult result;

        if (winnerPlayer == myPlayer.myTurn)
        {
            result = GameResult.victory;
        }
        else
        {
            result = GameResult.Fail;
        }

        endGamePanel.ShowPanel(result);
    }


    //IEnumerator GetGameData()
    //{
    //    HTTPRequest request = new HTTPRequest(new Uri(Constant.getBalanceDataURL), HTTPMethods.Get);
    //    request.Send();
    //    yield return StartCoroutine(request);
    //    JSONNode tempNode;
        
    //    if (request.Response.IsSuccess)
    //    {
    //        string ss = request.Response.DataAsText;
    //        tempNode = JSONNode.Parse(ss);
    //        balancedItemList = JsonMapper.ToObject<List<ItemMother>>(request.Response.DataAsText);
         
    //    }
    //    else
    //    {
    //        Debug.LogError("server problem");
    //    }

    //}

}
