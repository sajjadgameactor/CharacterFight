﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;
using LitJson;

public class PlayerScript : MonoBehaviour
{

    [SerializeField]
    public Text LeaderHealth,LeaderMana;

    [SerializeField]
    public HandDeckControllerScript handController;
    [SerializeField]
    public OnGroundCardManagerScript onGroundCardController;

    public PlayerScript rivalPlayer { get; private set; }
    int _Mana = 0;

    public int mana
    {
        get { return _Mana; }
        set
        {
            _Mana = Mathf.Min(value, 10);
            LeaderMana.text = _Mana.ToString();
        }

    }

    public Turn myTurn { get; private set; }

    [SerializeField]
    LeaderScript leader;

    // Use this for initialization
    void Start()
    {
    }

    public void SetPlayer(Turn turn,CardInfo myLeader,PlayerScript rival)
    {
        myTurn = turn;
        rivalPlayer = rival;
        leader.SetLeader(myLeader, this);
        LeaderHealth.text = myLeader.currentHealth.ToString();
    }

    public void addCard2Hand(CardInfo card)
    {
        CardScript temp = (Instantiate(Resources.Load<GameObject>("CardObject"))).GetComponent<CardScript>();
        temp.SetCard(card, this);
        temp.button.onClick.AddListener(delegate { requestCard2Ground(temp); });
        handController.AddCard2Hand(temp);
        //temp.OnCardClick += OnCardClick;
        //temp.SetColor(myColor);
    }

    Vector3 scaleStrange = new Vector3(1, 1, 0);
    public void requestCard2Ground(CardScript card)
    {
        if(onGroundCardController.HasCapacity())
        {
            if (card.myInfo.mana <= mana)
                GameSceneScript.Instance.gameLogicScript.Hand2GroundRequest(card.myInfo.cardID, myTurn);
            else
            {
                card.transform.DOComplete();
                card.transform.DOShakeScale(0.33f, scaleStrange).SetLoops(1);
                GameSceneScript.Instance.messegePanel.ShowMessege("انرژی آبی کافی نیست".faConvert(), 2);
            }
        }
        else
        {
            card.transform.DOComplete();
            card.transform.DOShakeScale(0.33f, scaleStrange).SetLoops(1);
            GameSceneScript.Instance.messegePanel.ShowMessege("حداکثر کارت در زمین قرار دارد".faConvert(), 2);
        }
      
    }

    public AttakableObjectScript FindAttackableObject(int id)
    {
        if (id == leader.myInfo.cardID)
            return leader;
        else
            return onGroundCardController.FindCard(id);
    }

    public bool CheckCanCardAttack(CardScript card)
    {
        if(!card.myInfo.CanAttack())
        {
            GameSceneScript.Instance.messegePanel.ShowMessege("در این نوبت نمیتواند حمله کند".faConvert(), 3);
            return false;
        }
        if(card.victamObject.myInfo.isHidden)
        {
            GameSceneScript.Instance.messegePanel.ShowMessege("تا زمانی که کارت مخفی حمله نکرده نمیتوان به آن حمله کرد".faConvert(), 3);
            return false;
        }

        if( !card.victamObject.myInfo.isDefender && rivalPlayer.CheckHasDefenderCard())
        {
            GameSceneScript.Instance.messegePanel.ShowMessege("فقط به کارت مدافع میتوانید حمله کنید".faConvert(), 3);
            return false;
        }

        return true;
    }

    public bool CheckHasDefenderCard()
    {
        foreach (var item in onGroundCardController.groundCard)
        {
            if (item.myInfo.isDefender)
                return true;
        }

        return false;
    }

    public void ShowHand()
    {
        handController.gameObject.SetActive(true);
    }

   

    public void HideHand()
    {
        handController.gameObject.SetActive(false);

    }

}
