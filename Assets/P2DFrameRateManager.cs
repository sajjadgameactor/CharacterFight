﻿using UnityEngine;
using System.Collections;

public class P2DFrameRateManager : MonoBehaviour {
    
    private enum FrameRateState
    {
        LowFrame,HighFrame
    }

    static FrameRateState frameRateState = FrameRateState.HighFrame;

    static float highFrameRateTime = 0;

    static int lowFrameRateNumber = 8;
    static int highFrameRateNumber = 30;

    static int maxFrameRateNumber = 60;

   public static  bool freezTime = false; // freez Time to stay in HighFrame

	// Use this for initialization
	void Start ()
    {
        QualitySettings.vSyncCount = 0;

	    if(Screen.currentResolution.width > 720)
        {
            maxFrameRateNumber = 60;
        }
        else
        {
            maxFrameRateNumber = 30;
        }

        SetHighFrameRate(maxFrameRateNumber, 3);

    }

    // Update is called once per frame
    void Update ()
    {
	    if(frameRateState == FrameRateState.HighFrame && !freezTime)
        {
            highFrameRateTime -= Time.deltaTime;

            if(highFrameRateTime <=0)
            {
                highFrameRateTime = 0;

                SetFrameRate(lowFrameRateNumber);
                frameRateState = FrameRateState.LowFrame;
            }
        }

	}


    static void SetFrameRate(int frameRate)
    {
        Application.targetFrameRate = Mathf.Min(frameRate, maxFrameRateNumber);
    }

    public static void SetHighFrameRate(int frameRate, float time)
    {
        if (frameRateState == FrameRateState.LowFrame)
        {
            highFrameRateTime = time;
            highFrameRateNumber = frameRate;
            SetFrameRate(highFrameRateNumber);
        }
        else
        {
            if (highFrameRateTime < time)
                highFrameRateTime = time;

            if (highFrameRateNumber < frameRate)
            {
                highFrameRateNumber = frameRate;
                SetFrameRate(highFrameRateNumber);
            }
        }

        frameRateState = FrameRateState.HighFrame;
      
        
    }

}
